using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class JugadorController : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private int estado = 0;
    private float velocidad = 10f;
    private int vida = 3;
    public Text life;

    public GameObject BalaPrefrab;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        life.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        estado = 0;
        animator.SetInteger("Estado", estado);
        rb.velocity = new Vector2(0, rb.velocity.y);

        life.text = "Vida: " + vida.ToString();

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            estado = 1;
            animator.SetInteger("Estado", estado);
            sprite.flipX = true;
            rb.velocity = new Vector2(velocidad * -1, rb.velocity.y);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            estado = 1;
            animator.SetInteger("Estado", estado);
            sprite.flipX = false;
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            estado = 2;
            animator.SetInteger("Estado", estado);
            rb.velocity = new Vector2(velocidad, 45);
        }

        if (Input.GetKeyUp(KeyCode.X))
        {
            estado = 3;
            animator.SetInteger("Estado", estado);
            Disparar(BalaPrefrab);
        }

        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.DownArrow))
        {
            estado = 4;
            animator.SetInteger("Estado", estado);
            sprite.flipX = false;
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
        }

        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.DownArrow))
        {
            estado = 4;
            animator.SetInteger("Estado", estado);
            sprite.flipX = true;
            rb.velocity = new Vector2(velocidad * -1, rb.velocity.y);
        }

        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKeyDown(KeyCode.X))
        {
            estado = 5;
            animator.SetInteger("Estado", estado);
            sprite.flipX = false;
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
            Disparar(BalaPrefrab);            
        }

        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.X))
        {
            estado = 5;
            animator.SetInteger("Estado", estado);
            sprite.flipX = true;
            rb.velocity = new Vector2(velocidad * -1, rb.velocity.y);
            Disparar(BalaPrefrab);
        }

    }

    private void Disparar(GameObject balas)
    {
        var x = this.transform.position.x;
        var y = this.transform.position.y;

        var bala = Instantiate(balas, new Vector2(x, y), Quaternion.identity);

        if (sprite.flipX)
        {
            var controller = balas.GetComponent<BalaController>();
            var flip = bala.GetComponent<SpriteRenderer>();
            controller.velocidad = controller.velocidad * -1;
            flip.flipX = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemigo")
        {
            if (vida == 1)
            {
                estado = 6;
                animator.SetInteger("Estado", estado);
                SceneManager.LoadScene("Escena_1");
            }
            else
            {
                vida = vida - 1;
            }
        }
    }
}
