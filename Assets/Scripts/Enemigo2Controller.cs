using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2Controller : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private float velocidad = 6f * -1;
    private int vida = 4;
    private bool flip = true;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Estado", 0);
        sprite.flipX = flip;
        rb.velocity = new Vector2(velocidad, rb.velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bala")
        {
            if (vida == 1)
            {
                animator.SetInteger("Estado", 1);
                Destroy(this.gameObject);
            }
            else
            {
                vida = vida - 1;
            }
        }
        if (collision.gameObject.tag == "Limite")
        {
            if (sprite.flipX)
            {
                flip = false;
                velocidad = 6f;
            }
            else
            {
                flip = true;
                velocidad = 6f * -1;
            }
        }
    }
}
