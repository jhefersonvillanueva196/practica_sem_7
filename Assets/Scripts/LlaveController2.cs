using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LlaveController2 : MonoBehaviour
{
    private CircleCollider2D cc;
    public Text fin;
    private bool verifica = true;

    void Start()
    {
        cc = GetComponent<CircleCollider2D>();
        fin = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (verifica == false)
        {
            fin.color = Color.white;
            Time.timeScale = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Jugador")
        {
            Debug.Log("Si choco");
            verifica = false;
        }
    }
}
